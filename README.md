
This module exists just to test loading remote modules.  You don't need to clone this project, just 
create a runnable go module with this code:

    package main

    import (
        "fmt"

        "gitlab.com/jdbgolang/greetings"
    )

    func main() {
        message := greetings.Hello("Jon")
        fmt.Println(message)
    }

Follow these steps
- `mkdir hello && cd hello`
- Copy the code above into `hello.go`
- `go mod init example.com/hello`
- `go tidy`
- `go run .`
